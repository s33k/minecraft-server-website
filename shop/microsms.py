from urllib.request import urlopen
from .mcrcon import *

rcon_ip = 'kills.pl'
rcon_port = 25575
rcon_password = 'n7ZQtPrHkxIR1emz'

class Value():
    def __init__(self, field, value):
        self.field = field
        self.value = value


class MicroTransfer():
    inputs = []
    url = 'https://microsms.pl/api/bankTransfer'
    ips = 'https://microsms.pl/psc/ips/'

    def add(self, field, value):
        self.inputs.append(Value(field, value))

    def validate_ipn(self, request):
        ip = get_client_ip(request)
        
        response = urlopen(self.ips)
        data = response.read()
        data = str(data)
        
        data = data.replace('b', '')
        data = data.replace("'", '')
        
        array = data.split(',')

        if not ip in array:
            raise Exception('Access denid.')

    def validate_user(self, userid, post_id):
        if userid != post_id:
            raise Exception('Bad user')


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
