from django import forms

from .models import Product


class CreateVoucher(forms.Form):
    products = Product.objects.all()

    choice = []
    for i in products:
        choice.append((i.id, i.name))

    product = forms.ChoiceField(choices=choice)
    buyer = forms.CharField(max_length=100)
    number = forms.IntegerField()


class UseVoucher(forms.Form):
    username = forms.CharField(max_length=50)
    code = forms.CharField(max_length=6)


class Sms(forms.Form):
    username = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-control'}))
    code = forms.CharField(max_length=8, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Tutaj wpisz kod z sms`a'}))


class Transfer(forms.Form):
    username = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))
