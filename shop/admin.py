from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(Product)
admin.site.register(Voucher)
admin.site.register(Sms_price)
admin.site.register(PaymentHistory)
