import json
import re

from django.contrib.auth.decorators import login_required, permission_required
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils.crypto import get_random_string
from urllib.request import urlopen
from hashlib import sha256

from config.models import Rcon, MicroSms
from .forms import *
from config.mcrcon import *
from .models import Voucher, Product, PaymentHistory
from .microsms import MicroTransfer

user_id = 3966
service_id = 4730


# Create your views here.
@login_required(login_url='home')
@permission_required('voucher.can_create', login_url='home')
def create_vouchers(request):
    if request.method == 'POST':
        voucher_form = CreateVoucher(request.POST)
        print('post')
        if voucher_form.is_valid():
            print('dobry formularz')
            number = int(request.POST['number'])

            array = []

            for i in range(0, number):
                print(request.POST['number'])
                voucher = Voucher()
                product_name = request.POST['product']
                product_ref = Product.objects.get(id=product_name)
                voucher.product = product_ref
                voucher.customer = request.POST['buyer']
                random = get_random_string(6)
                code = random
                array.append(random)
                voucher.code = code
                voucher.save()

            info = "Vouchers: "

            for i in range(0, len(array)):
                info += array[i] + ', '

        else:

            info = 'Error'

    else:
        voucher_form = CreateVoucher()
        info = ''

    return render(request, 'shop/create_vouchers.html', {'form': voucher_form, 'info': info})


def use_voucher(request):
    voucher_use = UseVoucher()
    rcon_config = Rcon.objects.get()
    message = ''

    if request.method == "POST":
        voucher_use = UseVoucher(request.POST)

        if voucher_use.is_valid():
            try:
                code = request.POST['code']
                username = request.POST['username']
                voucher = Voucher.objects.get(code=code)

                if voucher and voucher.status == '0':
                    product = voucher.product
                    command = product.command
                    command = command.format(username)

                    rcon = MCRcon()
                    rcon.connect(rcon_config.host, rcon_config.port, rcon_config.password)
                    rcon.command(command)
                    rcon.disconnect()

                    voucher.status = '1'
                    voucher.save()
                elif voucher.status == '1':
                    message = 'Ten voucher został już wykorzystany'
                else:
                    message = 'Nie ma takiego vouchera'

            except Exception as error:
                message = error

    return render(request, 'shop/use_voucher.html', {'form': voucher_use, 'error': message})


def shop(request):
    products = Product.objects.all()

    return render(request, 'shop/market.html', {'products': products})


def detail(request, name, msg=''):
    product = Product.objects.get(name=name)
    sms_form = Sms()
    transfer_form = Transfer()
    msgF = msg

    return render(request, 'shop/detail.html',
                  {'product': product, 'msg': msgF, 'sms_form': sms_form, 'transfer_form': transfer_form})


def sms(request, name):
    product = Product.objects.get(name=name)
    rcon_config = Rcon.objects.get()
    microsms_config = MicroSms.objects.get()
    msg = ''

    if request.method == 'POST':

        print('im working')
        if not request.POST['code'] is None:

            if re.match(r'^[A-Za-z0-9]{8}$', request.POST['code']):

                response = urlopen('http://microsms.pl/api/v2/index.php?userid=' + str(microsms_config.user_id) + "&number=" + str(
                    product.sms_price.number) + "&code=" + request.POST['code'] + '&serviceid=' + str(microsms_config.sms_service_id))
                data = json.loads(response.read().decode('utf-8'))
                print(data)

                if not data:
                    msg = "Nie można nawiązać połączenia z serwerem płatności."

                if data['connect']:

                    if data['data']['status'] == 1:
                        msg = 'Twój kod jest prawidłowy! Dziękuejmy za zakupy'
                        # dopisz rcon

                        username = request.POST['username']

                        history = PaymentHistory()
                        history.username = username
                        history.product = product
                        history.value = product.sms_price.price
                        history.method = 'sms'
                        history.save()

                        command = product.command
                        command = command.format(username)

                        rcon = MCRcon()
                        rcon.connect(rcon_config.host, rcon_config.port, rcon_config.password)
                        rcon.command(command)
                        rcon.disconnect()

                    else:
                        msg = 'Przesłany kod jest nieprawidłowy 1'

            else:
                msg = 'Przesłany kod jest nieprawidłowy 2'

    return redirect('detail msg', name=name, msg=msg)


def transfer(request, name):
    transfer = MicroTransfer()
    microsms_config = MicroSms.objects.get()
    product = Product.objects.get(name=name)

    hash = microsms_config.transfer_hash

    if request.method == 'POST':
        signature = str(microsms_config.transfer_service_id) + hash + str(product.price)

        transfer.add('shopid', microsms_config.transfer_service_id)
        transfer.add('return_url', 'http://kills.pl/shop/transfer/' + product.name + '/')
        transfer.add('return_urlc', 'http://kills.pl/shop/checkTransfer/')
        transfer.add('description', product.name + ' | ' + request.POST['username'])
        transfer.add('amount', product.price)
        transfer.add('signature', sha256(signature.encode()).hexdigest())
        transfer.add('email', request.POST['email'])
        transfer.add('control', str(product.id) + '.p.' + request.POST['username'])

    return render(request, 'shop/transfer.html', {'transfer': transfer})


@csrf_exempt
def check_transfer(request):
    transfer = MicroTransfer()
    microsms_config = MicroSms.objects.get()
    rcon_config = Rcon.objects.get()

    if request.method == 'POST':
        # check status
        if request.POST['status']:
            # seciurity
            transfer.validate_ipn(request)
            transfer.validate_user(str(microsms_config.user_id), str(request.POST['userid']))

            # do something if request is good
            info = request.POST['control']

            array = info.split('.p.')

            product_id = int(array[0])
            username = str(array[1])
            product = Product.objects.get(id=product_id)

            history = PaymentHistory()
            history.username = username
            history.product = product
            history.value = request.POST['amountPay']
            history.method = 'transfer'
            history.save()

            command = product.command
            command = command.format(username)

            rcon = MCRcon()
            rcon.connect(rcon_config.host, rcon_config.port, rcon_config.password)
            rcon.command(command)
            rcon.disconnect()

    return HttpResponse('OK')


def transfer_end(request, name):
    product = Product.objects.get(name=name)
    microsms_config = MicroSms.objects.get()

    msg = ''

    if request.method == 'GET':

        if not request.GET['status'] is None and not request.GET['hash'] is None and not request.GET['orderID'] is None:

            hash = request.GET['status'] + request.GET['orderID'] + microsms_config.transfer_hash

            if request.GET['hash'] == sha256(hash.encode()).hexdigest():
                if request.GET['status']:
                    msg = 'Płatność przebiegła poprawnie'
                else:
                    msg = 'Płatność przebiegła negatywnie1'
            else:
                msg = 'Płatność przebiegła negatywnie2'

    return render(request, 'shop/transfer_end.html', {'msg': msg})
