from django.db import models
from _datetime import datetime
from django.utils.crypto import get_random_string


class Sms_price(models.Model):
    number = models.IntegerField()
    price = models.IntegerField()
    price_vat = models.DecimalField(decimal_places=2, max_digits=6)

    def __str__(self):
        return str(self.price)


class Product(models.Model):
    name = models.CharField(max_length=150, unique=True)
    description = models.TextField()
    image = models.ImageField(upload_to='my_static/images/', null=True)
    sms_price = models.ForeignKey(Sms_price, on_delete=models.SET_NULL, null=True)
    price = models.DecimalField(decimal_places=2, max_digits=6)
    command = models.CharField(max_length=200,
                               help_text='W miejsce nazwy użytkownika wpisz \'{}\' żeby komenda zadziałała!')

    def __str__(self):
        return self.name

    class Meta:
        permissions = (('can_create', 'Create Vouchers'),)


class Voucher(models.Model):
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    customer = models.CharField(max_length=50)
    code = models.CharField(max_length=6)
    status = models.CharField(max_length=1, choices=(('0', 'ready'), ('1', 'used')), default='0')

    def __str__(self):
        return self.code


class PaymentHistory(models.Model):
    username = models.CharField(max_length=50)
    product = models.ForeignKey(Product, on_delete=models.DO_NOTHING)
    value = models.DecimalField(decimal_places=2, max_digits=6)
    data = datetime.now().strftime("%d %B %Y %H:%M")
    method = models.CharField(max_length=8, choices=(('transfer', 'transfer'), ('sms', 'sms')), default='sms')

    def __str__(self):
        return self.username + " " + str(self.data)