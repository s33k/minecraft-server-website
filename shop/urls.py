from django.urls import path

from . import views

urlpatterns = [
    path('create_voucher/', views.create_vouchers, name='create_vouchers'),
    path('use_voucher/', views.use_voucher, name='use_voucher'),
    path('', views.shop, name='shop'),
    path('detail/<str:name>/', views.detail, name='detail'),
    path('detail/<str:name>/msg/<str:msg>/', views.detail, name='detail msg'),
    path('detail/<str:name>/sms', views.sms, name='sms'),
    path('detail/<str:name>/transfer/', views.transfer, name='transfer'),
    path('checkTransfer/', views.check_transfer, name='check transfer'),
    path('transfer/<str:name>/', views.transfer_end, name='transfer end'),
]