from django.db import models


class TeamGroup(models.Model):
    name = models.CharField(max_length=50)
    color = models.CharField(max_length=50, choices=(('#1565c0', 'blue'), ('#424242', 'gray'),
                                                     ('#00e676', 'green'), ('#ef5350', 'red'),
                                                     ('#ffff00', 'yellow'), ('#4fc3f7', 'light blue'),
                                                     ('#ffffff', 'white')), default='primary')

    def __str__(self):
        return self.name


class TeamMember(models.Model):
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='my_static/members/', null=True)
    group = models.ForeignKey(TeamGroup, on_delete=models.DO_NOTHING, null=False)
    description = models.TextField()

    def __str__(self):
        return str(self.name)


class Top(models.Model):
    name = models.CharField(max_length=50, help_text="Nie pisz TOP ani TOPKA na początku!")
    command = models.CharField(max_length=50)

    def __str__(self):
        return 'TOP ' + self.name
