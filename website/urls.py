from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name="home"),
    path('regulamin/', views.rules, name='rules'),
    path('ekipa/', views.team, name='team'),
    path('topki/', views.top, name='tops'),
    path('informacje/', views.info, name='info'),
]