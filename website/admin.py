from django.contrib import admin

from .models import *

# Register your models here.
admin.site.register(TeamGroup)
admin.site.register(TeamMember)
admin.site.register(Top)