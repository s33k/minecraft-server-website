from django.shortcuts import render
from article.models import Article

from .models import *
from config.models import Rcon
from config.mcrcon import *


# Create your views here.
def home(request):
    articles = Article.objects.all()[:5]
    for article in articles:
        article.short = (article.body[:150] + '..') if len(article.body) > 150 else article.body
    return render(request, 'home.html', {'articles': articles})


def rules(request):
    return render(request, 'rules.html')


def team(request):
    members = TeamMember.objects.all()
    return render(request, 'team.html', {'members': members})


def top(request):
    rcon_config = Rcon.objects.get()
    tops = Top.objects.all()

    list = []

    rcon = MCRcon()
    rcon.connect(rcon_config.host, rcon_config.port, rcon_config.password)

    for top in tops:
        response = rcon.command(top.command)
        list_top = response.replace('\n', ' ').split(' ')
        mylist = []

        for a in list_top:
            if a.startswith('§c'):
                a = a.replace('§c', '')
                mylist.append(a)

        mylist[0] = top.name
        list.append(mylist)

    rcon.disconnect()

    return render(request, 'tops.html', {'tops': list})


def info(request):
    return render(request, 'info.html')