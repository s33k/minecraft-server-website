from django.http import Http404
from django.shortcuts import render
from .models import Article


# Create your views here.
def show(request, id):
    try:
        article = Article.objects.get(title=id)
    except Article.DoesNotExist:
        raise Http404('Taki Artykuł nie istnieje')
    return render(request, 'article/show.html', {'article': article})
