import os
from _datetime import datetime

from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Article(models.Model):
    title = models.CharField(max_length=200, unique=True)
    category = models.ForeignKey(Category, models.SET_NULL, null=True)
    image_header = models.ImageField(null=True)
    body = models.TextField()
    create_date = datetime.now().strftime("%d %B %Y %H:%M")
    user = models.ForeignKey(User, models.SET_NULL, null=True)

    def __str__(self):
        return self.title
