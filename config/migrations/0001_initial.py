# Generated by Django 2.1.7 on 2019-03-21 21:54

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MicroSms',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_id', models.IntegerField()),
                ('sms_service_id', models.IntegerField()),
                ('sms_code', models.CharField(max_length=15)),
                ('transfer_service_id', models.IntegerField()),
                ('transfer_hash', models.CharField(max_length=250)),
                ('encrypting', models.CharField(choices=[('md5', 'md5'), ('sha256', 'sha256')], default='md5', max_length=6)),
            ],
        ),
        migrations.CreateModel(
            name='Rcon',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('host', models.CharField(max_length=50)),
                ('port', models.IntegerField()),
                ('password', models.CharField(max_length=50)),
            ],
        ),
    ]
