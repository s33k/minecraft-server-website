from django.db import models


# Create your models here.
class Rcon(models.Model):
    host = models.CharField(max_length=50)
    port = models.IntegerField()
    password = models.CharField(max_length=50)

    def __str__(self):
        return self.host


class MicroSms(models.Model):
    user_id = models.IntegerField()
    sms_service_id = models.IntegerField()
    sms_code = models.CharField(max_length=15)
    transfer_service_id = models.IntegerField()
    transfer_hash = models.CharField(max_length=250)
    encrypting = models.CharField(max_length=6, choices=(('md5', 'md5'), ('sha256', 'sha256')), default='md5')

    def __str__(self):
        return str(self.user_id)
